FROM node:20-alpine AS builder
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm ci
COPY . .
RUN npm run build

FROM caddy:2-alpine

ARG VERSION=dev

WORKDIR /usr/share/caddy
COPY --from=builder /app/dist ./dist
COPY --from=builder /app/plugin ./plugin
COPY --from=builder /app/index.html .
RUN sed -i "s/{{VERSION}}/${VERSION}/g" index.html
